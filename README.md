# Message Pack Extensions - Typescript

This is the typescript library (and also the reference library) of the extensions to MessagePack I use.

## MessagePack: Safe
This is a modification of MessagePack that includes Map objects and Set objects as being valid to convert over.

Is it overbuilt? Yes. Am I keeping it? Also Yes. I prefer it being overbuilt and *never* needing to touch it to ever having to update it again.

## MessagePack: Simple-Data-Store
This is a simple data storage format for storing data using the `MessagePack: Safe` format extensions.

### Access Methods
The various access methods the SDS system can plug into.

Currently supported:
* DENO / Linux.

## UserConf / LocalConf
A user or device configuration version of the SDS. Intended to be used to save the path of a program's main SDS location along with other core configuration details. (Such as version of SDS data.)