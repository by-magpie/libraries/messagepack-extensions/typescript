import type { MpSafe } from "../mp-safe/mod.ts";
import type { GenericStorageInterface } from "./storage-methods/base-fs.ts";
import { DataStore, openOrMakeStore, openStore, makeStore, TypeMismatchError } from "./data-store.ts";
import { GenericZipStorageInterface } from "./storage-methods/zip-fs/base-zip-fs.ts";
function ssg(g: unknown): g is Set<string> {
    if(g instanceof Set){
        for(const e of g){
            if(typeof e != "string")
                return false;
        }
        return true;
    }
    return false;
}

function unknownGuard(_u: unknown): _u is unknown {
    return true;
}

export class DataStoreManager {
    static ReservedTableNames: Map<string, string> = new Map([
        ["TableIndex", "__storeIndex"],
        ["CleanupIndex", "__cleanupIndex"],
    ]);
    private static fileExt = "smpk"
    private readonly fs: GenericStorageInterface;
    private readonly stores: DataStore<Set<string>>;
    private readonly cleanupList: DataStore<Set<string>>;
    protected readonly smp: MpSafe;
    private constructor(
        storesIndex: DataStore<Set<string>>,
        cleanupIndex: DataStore<Set<string>>,
        smp: MpSafe,
        fs: GenericStorageInterface
        ) {
        this.stores = storesIndex;
        this.cleanupList = cleanupIndex;
        this.smp = smp;
        this.fs = fs;
    }

    private static async bootstrapIndex(
        smp: MpSafe,
        fs: GenericStorageInterface,
        indexName: string,
    ){
        const index = DataStoreManager.ReservedTableNames.get(indexName);
        if(index == undefined){
            throw new Error("Invalid bootstrap index!");
        }
        const filePath = `/${index}.${DataStoreManager.fileExt}`;
        try{
            const indexStore = await openOrMakeStore<Set<string>>(
                filePath, smp, fs, ssg, new Set<string>()
            );
            return indexStore;
        } catch(e) {
            if(e instanceof DataStore.errors.TypeMismatchError){
                throw new Error("Corrupt " + indexName);
            } else {
                throw e;
            }
        }
    }

    static async init(
        smp: MpSafe,
        fs: GenericStorageInterface,
        ) {
        fs.ensureDir("");
        const [storeIndex, cleanupIndex] = await Promise.all([
            DataStoreManager.bootstrapIndex( smp, fs, "TableIndex"),
            DataStoreManager.bootstrapIndex( smp, fs, "CleanupIndex")
        ]);
        return new DataStoreManager(storeIndex, cleanupIndex, smp, fs);
    }

    private async updateIndex() {
        await Promise.all([
            this.stores.flush(),
            this.cleanupList.flush(),
        ]);
    }

    public async newStore<T>(
        name: string,
        baseData: T,
        guard: (g:unknown) => g is T,
        force = false
        ): Promise<DataStore<T> | null> {
        if(this.stores.data.has(name) && force == false){
            return null;
        }
        this.stores.data.add(name);
        await this.updateIndex();
        const [store, _update] = await Promise.all([
            makeStore<T>(
            `/${name}.${DataStoreManager.fileExt}`, this.smp, this.fs, guard,
            baseData
            ),
            this.updateIndex()
        ]);
        await store.flush();
        return store;
    }

    public async newOrOpenAndRepairStore<T>(
        name: string,
        fallbackData: T,
        guard: (g:unknown) => g is T,
        repair: (g:unknown) => T,
        force = false,
        ): Promise<DataStore<T>>{
        if(this.cleanupList.data.has(name)){
            await this.deleteStore(name);
            this.cleanupList.data.delete(name);
            this.updateIndex();
        }
        if(this.stores.data.has(name) && force == false){
            return await this.openRepairStore<T>(name, guard, repair);
        }
        const store = await this.newStore(
            name,
            fallbackData,
            guard,
            true,
        );
        if(store){
            return store;
        }
        throw new Error("Unreachable Line.");
    }

    public async newOrOpenStore<T>(
        name: string,
        fallbackData: T,
        guard: (g:unknown) => g is T,
        force = false,
        ): Promise<DataStore<T>>{
        if(this.cleanupList.data.has(name)){
            await this.deleteStore(name);
            this.cleanupList.data.delete(name);
            this.updateIndex();
        }
        if(this.stores.data.has(name) && force == false){
            return await this.openStore<T>(name, guard);
        }
        const store = await this.newStore(
            name,
            fallbackData,
            guard,
            true,
        );
        if(store){
            return store;
        }
        throw new Error("Unreachable Line.");
    }

    async openRepairStore<T>(
        name: string,
        guard: (g: unknown) => g is T,
        repair: (g: unknown) => T,
    ): Promise<DataStore<T>> {
        const tmpStore = await openStore<unknown>(
            `/${name}.${DataStoreManager.fileExt}`,
            this.smp,
            this.fs,
            unknownGuard
            );
        tmpStore.data = repair(tmpStore.data);
        await tmpStore.flush();
        return openStore<T>(
            `/${name}.${DataStoreManager.fileExt}`,
            this.smp,
            this.fs,
            guard
            );
    }

    public openStore<T>(
        name: string,
        guard: (g: unknown) => g is T
        ): Promise<DataStore<T>> {
        return openStore<T>(
            `/${name}.${DataStoreManager.fileExt}`,
            this.smp,
            this.fs,
            guard
            );
    }

    private deleteStore(name: string) {
        return this.fs.remove(`/${name}.${DataStoreManager.fileExt}`);
    }

    public dropStore(name: string) {
        if(this.stores.data.has(name)){
            this.cleanupList.data.add(name);
            this.stores.data.delete(name);
        }
    }

    public async cleanup() {
        const waitList: Array<Promise<boolean | void>|boolean> = []
        for(const name of this.cleanupList.data){
            waitList.push(this.deleteStore(name));
        }
        this.cleanupList.data.clear();
        waitList.push(this.updateIndex());
        await Promise.all(waitList);
    }

    public flush() {
        return this.updateIndex();
    }

    public has(name: string): "null" | "exists" | "dropped" {
        if(this.stores.data.has(name))
            return "exists";
        if(this.cleanupList.data.has(name))
            return "dropped";
        return "null";
    }

    public verify<T>(
        name: string,
        guard: (g: unknown) => g is T
        ): boolean {
            try {
                openStore<T>(
                    `/${name}.${DataStoreManager.fileExt}`,
                    this.smp,
                    this.fs,
                    guard
                );
                return true;
            } catch(e) {
                if(e instanceof TypeMismatchError){
                    return false;
                }
            }
            throw new Error("Unreachable Line in dsm.verify!?");
        }

    public *cycleStoreNames(): Generator<string, void> {
        for(const store of this.stores.data){
            yield(store);
        }
    }

    public async insertToZip(zipFS: GenericZipStorageInterface<unknown>) {
        zipFS.ensureRoot();
        const indexName = DataStoreManager.ReservedTableNames.get("TableIndex");
        const indexP = this.fs.readFile(
                `/${indexName}.${DataStoreManager.fileExt}`)
            .then((f) => {
                zipFS.writeFile(`/${indexName}.${DataStoreManager.fileExt}`, f)
            });
        const cleanupName = DataStoreManager.ReservedTableNames.get("CleanupIndex");
        const droppedP = this.fs.readFile(
                `/${cleanupName}.${DataStoreManager.fileExt}`)
            .then((f) => {
                zipFS.writeFile(`/${cleanupName}.${DataStoreManager.fileExt}`, f)
            });
        const transferSet: Promise<void>[] = [indexP, droppedP]
        for(const name of this.cycleStoreNames()){
            const p = this.fs.readFile(
                    `/${name}.${DataStoreManager.fileExt}`)
                .then((f) => {
                    zipFS.writeFile(`/${name}.${DataStoreManager.fileExt}`, f)
                });
            transferSet.push(p);
        }
        await Promise.all(transferSet);
        return;
    }
}