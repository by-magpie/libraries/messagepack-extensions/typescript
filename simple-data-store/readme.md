# Simple-Data-Store
This is a rewrite of a older library I had made for simple data storage. It's meant to be a general use library for when someone is too lazy to write it properly.

## storage-methods
These are meant to provide a p-n-p system for different storage methods while using the same data-formatting.

* deno-fs: Real FS access in Deno.  
  * currently linux only.
* node-fs: Real FS access in Node. (Unimplemented)
* ipfs-fs: FS via IPFS. (Unimplemented)
* html-fs: Uses fetch requests. (Untested read only, unimplemented write access)
* zip-fs: different zip fs systems.  
  * Currently the only variant is based off.