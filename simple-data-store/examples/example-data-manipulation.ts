import type {DataStoreManager} from "../mod.ts";

type Safe = Map<string, Set<number>>;

function noErrors(a: string[] = []): string[]{
    if(a.length == 0){
        return ["No errors."]
    }
    return a;
}
function exampleGuard(data: unknown): data is Safe {
    if(!(data instanceof Map))
        return false;
    for(const [k, v] of data){
        if(typeof k !== "string")
            return false;
        if(!(v instanceof Set))
            return false;
        for(const i of v){
            if(typeof i != "number")
                return false;
        }
    }
    return true;
}

const exampleData: Safe = new Map<string, Set<number>>();
exampleData.set("exampleA", new Set([0, 1, 2, 3]));
exampleData.set("exampleB", new Set([4, 3, 2, 1]));
const exampleAddition:[string, Set<number>] = ["exampleC", new Set([3, 2, 1, 0])];

const strNum = [
    "example-dropped",
    "example-kept",
    "example-added",
    "example-new",
]


export async function step0(dsm: DataStoreManager) {
    for(const name of strNum){
        dsm.dropStore(name);
    }
    await dsm.cleanup();
    await dsm.flush();
    return noErrors();
}

export async function check0(dsm: DataStoreManager) {
    const failed = [];
    await null;
    if(dsm.has(strNum[0]) != "null")
        failed.push(`${strNum[0]} != null`);
    if(dsm.has(strNum[1]) != "null")
        failed.push(`${strNum[1]} != null`);
    if(dsm.has(strNum[2]) != "null")
        failed.push(`${strNum[2]} != null`);
    if(dsm.has(strNum[3]) != "null")
        failed.push(`${strNum[3]} != null`);
    return noErrors(failed);
}

export async function step1(dsm: DataStoreManager){
    const drop = await dsm.newOrOpenStore(strNum[0], exampleData, exampleGuard);
    const keep = await dsm.newOrOpenStore(strNum[1], exampleData, exampleGuard);
    const added = await dsm.newOrOpenStore(strNum[2], exampleData, exampleGuard);
    await drop.flush();
    await keep.flush();
    await added.flush();
    await dsm.flush();
    return noErrors();
}
export async function check1(dsm: DataStoreManager) {
    const failed = [];
    if(dsm.has(strNum[0]) != "exists")
        failed.push(`${strNum[0]} != exists`);
    if(dsm.has(strNum[1]) != "exists")
        failed.push(`${strNum[1]} != exists`);
    if(dsm.has(strNum[2]) != "exists")
        failed.push(`${strNum[2]} != exists`);
    if(dsm.has(strNum[3]) != "null")
        failed.push(`${strNum[3]} != null`);
    const added = await dsm.openStore(strNum[2], exampleGuard);
    if(added.data.has("exampleC"))
        failed.push(`${strNum[2]} has exampleC`);
    return noErrors(failed);
}

export async function step2(dsm: DataStoreManager) {
    const add = await dsm.openStore(strNum[2], exampleGuard);
    add.data.set(exampleAddition[0], exampleAddition[1]);
    await add.flush();
    await dsm.newOrOpenStore(strNum[3], exampleData, exampleGuard);
    dsm.dropStore(strNum[0]);
    await dsm.flush();
    return noErrors();
}

export async function check2(dsm: DataStoreManager) {
    const failed = [];
    if(dsm.has(strNum[0]) != "dropped")
        failed.push(`${strNum[0]} != dropped`);
    if(dsm.has(strNum[1]) != "exists")
        failed.push(`${strNum[1]} != exists`);
    if(dsm.has(strNum[2]) != "exists")
        failed.push(`${strNum[2]} != exists`);
    if(dsm.has(strNum[3]) != "exists")
        failed.push(`${strNum[3]} != exists`);
    const added = await dsm.openStore(strNum[2], exampleGuard);
    if(!added.verifyStoreSafety())
        failed.push(`${strNum[2]} is unsafe`);
    if(!added.data.has("exampleC"))
        failed.push(`${strNum[2]} missing exampleC`);
    return noErrors(failed);
}
export async function step3(dsm: DataStoreManager) {
    await dsm.cleanup();
    await dsm.newOrOpenStore(strNum[2], exampleData, exampleGuard);
    await dsm.flush();
    return noErrors();
}

export async function check3(dsm: DataStoreManager) {
    const failed = [];
    if(dsm.has(strNum[0]) != "null")
        failed.push(`${strNum[0]} != null`);
    if(dsm.has(strNum[1]) != "exists")
        failed.push(`${strNum[1]} != exists`);
    if(dsm.has(strNum[2]) != "exists")
        failed.push(`${strNum[2]} != exists`);
    if(dsm.has(strNum[3]) != "exists")
        failed.push(`${strNum[3]} != exists`);
    const added = await dsm.openStore(strNum[2], exampleGuard);
    if(!added.verifyStoreSafety())
        failed.push(`${strNum[2]} is unsafe`);
    if(!added.data.has("exampleC"))
        failed.push(`${strNum[2]} is missing exampleC`);
    return noErrors(failed);
}

export const stepsAndChecks:[ string, (dsm: DataStoreManager)=>Promise<string[]>][] = [
    [" step0", step0],
    ["check0", check0],
    [" step1", step1],
    ["check1", check1],
    [" step2", step2],
    ["check2", check2],
    [" step3", step3],
    ["check3", check3],
]