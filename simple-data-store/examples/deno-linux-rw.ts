#!/usr/bin/env -S deno run -A ./deno-linux-rw.ts
import {SafetyModules, MpSafe} from "../../mp-safe/mod.ts";
import {StorageMethods, DataStoreManager} from "../mod.ts";
import { stepsAndChecks } from "./example-data-manipulation.ts";
const smpExt: SafetyModules.BaseMpExtension[] = [
    new SafetyModules.SetMpExtension(),
    new SafetyModules.MapMpExtension(),
]
const smp = new MpSafe(smpExt);
const storageMethod = new StorageMethods.DenoStorageInterface("./example-db", false);

const dsm = await DataStoreManager.init(smp, storageMethod);
console.log("deno-linux-fs")
for(const [name, func] of stepsAndChecks){
    const res = await func(dsm);
    console.log(`${name}: [${res.join(";")}]`);
}

/*for await(const item of storageMethod.walk(false)){
    console.log(item);
}*/