import {default as deepEql} from "https://deno.land/x/deep_eql@v5.0.1/index.js";
import type { MpSafe } from "../mp-safe/mod.ts";
import { GenericStorageInterface as fsGenInt } from "./storage-methods/base-fs.ts";
export async function openOrMakeStore<T>(
    path: string,
    smp: MpSafe,
    fs: fsGenInt,
    guard: (g:unknown) => g is T,
    seedData: T,
): Promise<DataStore<T>> {
    if(!seedData)
        throw new Error("Missing seedData Arg during 'makeStore'")
    try {
        return await openStore<T>(path, smp, fs, guard);
    } catch(e) {
        if(e instanceof fsGenInt.errors.NotFound){
            return makeStore<T>(path, smp, fs, guard, seedData);
        }
        throw e;
    }
}

export async function makeStore<T>(
    path: string,
    smp: MpSafe,
    fs: fsGenInt,
    guard: (g:unknown) => g is T,
    seedData: T,
): Promise<DataStore<T>> {
    if(!seedData)
        throw new Error("Missing seedData Arg during 'makeStore'")
    const store = new DataStore<T>(
        path,
        seedData,
        smp,
        fs,
        guard
        );
    await store.flush();
    return store;
}

export async function openStore<T>(
    path: string,
    smp: MpSafe,
    fs: fsGenInt,
    guard: (g:unknown) => g is T
): Promise<DataStore<T>> {
        return new DataStore<T>(
            path,
            await loadData<T>(path, smp, fs, guard),
            smp,
            fs,
            guard
            );
}

export async function loadData<T>(
    path: string,
    smp: MpSafe,
    fs: fsGenInt,
    guard: (g:unknown) => g is T
) {
    const fileStream = await fs.readFile(path);
    const data = smp.decode(fileStream);
    if(guard(data)){
        return data;
    } else {
        throw new DataStore.errors.TypeMismatchError(path, guard.name, "loadData");
    }
}

export class TypeMismatchError extends Error {
    readonly path: string;
    constructor(path: string, guard: string, event?: string) {
        let msg;
        if(event){
            msg = `Type mismatch on '${path}' (Guard Fail FN:${guard}) during ${event}`;
        } else{
            msg = `Type mismatch on '${path}' (Guard Fail FN:${guard})`;
        }
        super(msg);
        this.path = path;
    }
}
export class DataStore<T> {
    private readonly file: string;
    private readonly smp: MpSafe;
    private store: T;
    private readonly fs: fsGenInt;
    private readonly guard: (g:unknown) => g is T
    constructor(
        file: string,
        store: T,
        smp: MpSafe,
        fs:fsGenInt,
        guard: (g:unknown) => g is T
        ) {
        this.file = file;
        this.smp = smp;
        this.store = store;
        this.fs = fs;
        this.guard = guard;
    }
    flush() {
        return this.fs.writeFile(
            this.file,
            this.smp.encode(this.store)
        );
    }

    flushSafe() {
        if(this.verifyStoreSafety())
            return this.flush();
        else
            throw new DataStore.errors.TypeMismatchError(
                this.file,
                this.guard.name,
                "flushSafe"
            )
    }

    /**
     * Verifies encoding safety by attempting to encode then decode the data.
     * @returns true if it is safe to encode.
     */
    verifyStoreSafety(): boolean {
        const schrodinger = this.smp.decode(
            this.smp.encode(this.data)
        );
        const a = this.guard(schrodinger);
        const b = deepEql(this.data, schrodinger);
        return a && b;
    }
    async refresh() {
        this.data = await loadData<T>(this.file, this.smp, this.fs, this.guard);
    }

    get data() {
        return this.store;
    }

    set data(data: T) {
        this.store = data;
    } 
    static errors = {
        TypeMismatchError
    }
}