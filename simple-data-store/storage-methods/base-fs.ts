import { NotFound, PermissionDenied } from "./errors.ts";

export enum WalkEntryType {
    UNKNOWN           = 0b1000, // 8
    FILE              = 0b0010, // 2
    DIRECTORY         = 0b0100, // 4
    SYMLINK_UNKNOWN   = 0b1001, // 9
    SYMLINK_FILE      = 0b0011, // 3
    SYMLINK_DIRECTORY = 0b0101, // 5
}
export interface WalkEntry {
    type: WalkEntryType,
    relPath: string,
    absPath: string,
    name: string,
}
export abstract class GenericStorageInterface {
    readonly write: boolean;
    readonly root: string;
    constructor(root:string, readonly: boolean){
        this.write = !readonly;
        this.root = root;
    }
    abstract readFile(uriRel: string):  Promise<Uint8Array>;
    abstract writeFile(uriRel: string, data: Uint8Array): Promise<boolean> | boolean;
    abstract remove(uriRel: string, recursive?: boolean): Promise<boolean> | boolean;
    abstract ensureDir(uriRel: string): Promise<boolean> | boolean;
    abstract ensureRoot(): Promise<boolean> | boolean;
    abstract walk(ignoreSymLinks: boolean, uriRel?: string): AsyncIterableIterator<WalkEntry>;

    fxPath(uriRel: string): string {
        return this.root + uriRel;
    }

    public static errors = {
        NotFound,
        PermissionDenied
    }
}