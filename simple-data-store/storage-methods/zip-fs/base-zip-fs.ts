import { GenericStorageInterface } from "../base-fs.ts";
export 
    abstract class GenericZipStorageInterface<UZT>
        extends GenericStorageInterface {
    protected zipSystem: UZT | null = null;
    private underlyingFS: GenericStorageInterface
    private subRoot: string;
    constructor(
        root: string,
        readonly: boolean,
        underlyingFS: GenericStorageInterface,
        subRoot: string) {
            super(root, readonly);
            this.underlyingFS = underlyingFS;
            this.subRoot = subRoot;
        }
    async ensureRoot(){
        const blob = await this.underlyingFS.readFile(this.root);
        try{
            this.zipSystem = this.extractFromZip(blob);
        } catch(e) {
            throw e;
        }
        return true;
    }
    async replaceRoot() {
        if(!this.zipSystem)
            throw new Error("Invalid state")
            //TODO: Replace this with a proper error;
        const blob = this.rebuildZip();
        return await this.underlyingFS.writeFile(this.root, blob);
    }
    abstract extractFromZip(blob: Uint8Array): UZT;
    abstract rebuildZip(): Uint8Array;
    fxPath(uriRel: string): string {
      return this.subRoot + uriRel;
    }
}