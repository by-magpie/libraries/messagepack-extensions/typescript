export { GenericStorageInterface } from "./base-fs.ts";
export { DenoStorageInterface } from "./deno-fs.ts";
export { HttpStorageInterface } from "./http-fs.ts";