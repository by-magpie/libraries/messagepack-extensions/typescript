import { ensureDir } from "https://deno.land/std@0.196.0/fs/mod.ts";
import { GenericStorageInterface, WalkEntry, WalkEntryType } from "./base-fs.ts";
import { walk } from "https://deno.land/std@0.196.0/fs/mod.ts";
export class DenoStorageInterface extends GenericStorageInterface {
    async *walk(ignoreSymLinks: boolean, uriRel?: string | undefined): AsyncIterableIterator<WalkEntry> {
        const uri = uriRel ? this.fxPath(uriRel) : this.root;
        for await(const entry of walk(uri, {followSymlinks: !ignoreSymLinks})){
            let type = WalkEntryType.UNKNOWN
            if(entry.isSymlink){
                if(entry.isDirectory){
                    type = WalkEntryType.SYMLINK_DIRECTORY
                } else if(entry.isFile){
                    type = WalkEntryType.SYMLINK_FILE
                } else {
                    type = WalkEntryType.SYMLINK_UNKNOWN
                }
            } else {
                if(entry.isDirectory){
                    type = WalkEntryType.DIRECTORY
                } else if(entry.isFile){
                    type = WalkEntryType.FILE
                } else {
                    type = WalkEntryType.UNKNOWN
                }
            }
            const name = entry.name;
            const relPath = entry.path;
            const we: WalkEntry = {
              type,
              relPath,
              absPath: this.fxPath(relPath),
              name
            }
            yield we;
        }
    }
    async ensureDir(uriRel: string): Promise<boolean> {
        const uri = this.fxPath(uriRel);
        try{
            await ensureDir(uri)
        } catch(e) {
            if(e instanceof Deno.errors.PermissionDenied){
                throw new GenericStorageInterface.errors.PermissionDenied("ensure directory", uri, e);
            }
        }
        return true;
    }
    ensureRoot(): boolean | Promise<boolean> {
      return this.ensureDir("/");
    }
    async readFile(uriRel: string): Promise<Uint8Array> {
        const uri = this.fxPath(uriRel);
        try{
            return await Deno.readFile(uri);
        } catch(e) {
            if(e instanceof Deno.errors.NotFound){
                throw new GenericStorageInterface.errors.NotFound(uri, e);
            }
            if(e instanceof Deno.errors.PermissionDenied){
                throw new GenericStorageInterface.errors.PermissionDenied("read", uri, e);
            }
            throw e;
        }
    }
    async writeFile(uriRel: string, data: Uint8Array): Promise<boolean> {
        const uri = this.fxPath(uriRel);
        if(!this.write){
            return false;
        }
        try {
            await Deno.writeFile(uri, data);
            return true;
        } catch(e) {
            if(e instanceof Deno.errors.PermissionDenied){
                throw new GenericStorageInterface.errors.PermissionDenied("write", uri, e);
            }
            throw e;
        }
    }
    async remove(uriRel: string, recursive = false): Promise<boolean> {
        const uri = this.fxPath(uriRel);
        if(!this.write){
            return false;
        }
        try {
            await Deno.remove(uri, {recursive});
            return true
        } catch (e) {
            if(e instanceof Deno.errors.NotFound){
                throw new GenericStorageInterface.errors.NotFound(uri, e);
            }
            if(e instanceof Deno.errors.PermissionDenied){
                if(recursive){
                    throw new GenericStorageInterface.errors.PermissionDenied("delete recursively", uri, e);
                } else {
                    throw new GenericStorageInterface.errors.PermissionDenied("delete", uri, e);
                }
            }
            if(e.code == "ENOTEMPTY"){
                throw new GenericStorageInterface.errors.PermissionDenied("delete recursively", uri, e, "Not Recursive & Not Empty");
            }
            throw e;
        }
    }
}