export class NotFound extends Error {
    public readonly path: string;
    constructor(path: string, cause: Error, additional?: string) {
        let msg: string;
        if(additional){
            msg = `File ${path} Not found.`;
        } else {
            msg = `File ${path} Not found.`;
        }
        super(msg, {cause});
        this.path = path;
    }

}

export class PermissionDenied extends Error {
    public readonly path: string;
    constructor(
        action: string,
        path: string,
        cause: Error,
        additional?: string
    ) {
        let msg: string;
        if(additional){
            msg = `Insufficient permissions to ${action} ${path}. (${additional})`;
        } else {
            msg = `Insufficient permissions to ${action} ${path}.`;
        }
        super(msg, {cause});
        this.path = path;
    }

}