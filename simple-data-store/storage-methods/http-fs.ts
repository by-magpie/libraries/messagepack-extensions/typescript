import { GenericStorageInterface } from "./base-fs.ts";
export class HttpStorageInterface extends GenericStorageInterface {
    ensureDir(uriRel: string): boolean | Promise<boolean> {
      throw new Error("Method not implemented.");
    }
    constructor(_readonly: true){
        super(true);
    }
    async readFile(uri: string): Promise<Uint8Array> {
        try{
            const rq = await fetch(uri);
            if(rq.status == 404){
                throw new GenericStorageInterface.errors.NotFound(uri, new Error(`Received status code ${rq.status}`));
            }
            if(rq.status == 401 || rq.status == 403){
                throw new GenericStorageInterface.errors.PermissionDenied("fetch", uri, new Error(`Received status code ${rq.status}`));
            }
            if(rq.status != 200){
                throw new Error(`Received status code ${rq.status} instead of a 200!`)
            }
            const buffer = await rq.arrayBuffer();
            const typed = new Uint8Array(buffer);
            return typed;
        } catch(e) {
            if(e instanceof Deno.errors.NotFound){
                throw new GenericStorageInterface.errors.NotFound(uri, e);
            }
            if(e instanceof Deno.errors.PermissionDenied){
                throw new GenericStorageInterface.errors.PermissionDenied("read", uri, e);
            }
            throw e;
        }
    }
    async writeFile(_uri: string, _data: Uint8Array): Promise<boolean> {
        if(!this.write){
            return false;
        }
        return false;
        // deno-lint-ignore no-unreachable
        await null;
    }
    async remove(_uri: string, _recursive = false): Promise<boolean> {
        if(!this.write){
            return false;
        }
        return false;
        // deno-lint-ignore no-unreachable
        await null;
    }
}