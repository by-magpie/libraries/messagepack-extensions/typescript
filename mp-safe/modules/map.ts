import { ExtensionCodec } from "../../msgpack_import.ts";
import { BaseMpExtension } from "./base.ts";
const type = 1;
export class MapMpExtension extends BaseMpExtension {
  encode(object: unknown): Uint8Array | null {
    if (object instanceof Map) {
      return this.safeEncode([...object]);
    } else {
      return null;
    }
  }
  decode(data: Uint8Array) {
    const array = this.safeDecode(data) as Array<[unknown, unknown]>;
    return new Map(array);
  }
  install(codec: ExtensionCodec): void {
    this.installInner(codec, type);
  }
}
