import { ExtensionCodec, DecodeError } from "../../msgpack_import.ts";
import { BaseMpExtension } from "./base.ts";
const type = 2;

//FIXME: This does not preform a valid input/output when ran through message-pack. (valid when done directly, IDK why.)
export class BigIntMpExtension extends BaseMpExtension {
  encode(object: unknown): Uint8Array | null {
    if (typeof object === "bigint") {
      const typedArray = new BigInt64Array([object]);
      const finalArray = new Uint8Array(typedArray.buffer);
      console.log({finalArray, typedArray})
      return finalArray;
    } else {
      return null;
    }
  }

  decode(finalArray: Uint8Array): bigint {
    let typedArray = new BigInt64Array(finalArray.buffer, 0, 1);
    if(finalArray[0] == 215 && finalArray[1] == 2)
    typedArray = new BigInt64Array(finalArray.buffer, 2, 1);
    console.log({finalArray, typedArray})
    //return typedArray[0];
    throw new DecodeError("FIXME: This extension doesn't currently work.")
  }
  install(codec: ExtensionCodec): void {
    this.installInner(codec, type);
  }
}
