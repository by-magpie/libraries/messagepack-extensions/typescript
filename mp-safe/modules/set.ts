import { ExtensionCodec } from "../../msgpack_import.ts";
import { BaseMpExtension } from "./base.ts";
const type = 0;
export class SetMpExtension extends BaseMpExtension {
  encode(object: unknown): Uint8Array | null {
    if (object instanceof Set) {
      return this.safeEncode([...object]);
    } else {
      return null;
    }
  }
  decode(data: Uint8Array) {
    const array = this.safeDecode(data) as Array<unknown>;
    return new Set(array);
  }
  install(codec: ExtensionCodec): void {
    this.installInner(codec, type);
  }
}
