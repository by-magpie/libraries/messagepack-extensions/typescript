import { ExtensionCodec } from "../../msgpack_import.ts";
export abstract class BaseMpExtension {
  private unsafeEncode: ((data: unknown) => Uint8Array) | null = null;
  private unsafeDecode: ((data: Uint8Array) => unknown) | null = null;
  get safeEncode(): (data: unknown)=>Uint8Array {
    if(!this.unsafeEncode)
      throw new Error("Missed loadED!");
    return this.unsafeEncode
  }
  get safeDecode(): (data: Uint8Array) => unknown {
    if(!this.unsafeDecode)
      throw new Error("Missed loadED!");
    return this.unsafeDecode
  }
  constructor(
  ) {}
  loadED(
    safeEncode: (data: unknown) => Uint8Array,
    safeDecode: (data: Uint8Array) => unknown,
  ) {
    this.unsafeEncode = safeEncode;
    this.unsafeDecode = safeDecode;
  }

  abstract encode(object: unknown): Uint8Array | null;
  // deno-lint-ignore no-explicit-any
  abstract decode(data: Uint8Array): any;
  abstract install(codec: ExtensionCodec): void;
  installInner(codec: ExtensionCodec, type: number) {
    if (type < 0) {
      throw new RangeError("Type must be between 0-127");
    }
    if (type > 127) {
      throw new RangeError("Type must be between 0-127");
    }
    if(this.encode == null || this.decode == null)
      throw new Error("Missed loadED call!");
    codec.register({
      type,
      encode: (data) => this.encode(data),
      decode: (data) => this.decode(data),
    });
  }
}
