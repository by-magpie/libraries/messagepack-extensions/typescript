# Reserved ID's
This is a list of all the reserved ID's for the (my) safe version of MessagePack.

| ID | Type       | Description              | Recursive | Implemented |
|----|------------|--------------------------| -- |--|
|   0  | Set         | Array without duplicates |✔|✔|
|   1  | Strict-Map  | Guarantees a Map Object  |✔|✔|
|   2  | BigInt      | 64-bit Int's             |✖| Bad Impl. Needs re-work. |
|   3 | Reserved     |  | | |
| ... | ...
|  31 | Reserved     |  | | |
|  32 | Unused       |  | | |
| ... | ...          |  | | |
| 119 | Unused       |  | | |
| 120 | RecursiveExt | Used to extend the extension namespace |✔|✖|
| ... | ...          |  | | |
| 127 | RecursiveExt | Used to extend the extension namespace |✔|✖|
| 128 | Invalid      |  | | |