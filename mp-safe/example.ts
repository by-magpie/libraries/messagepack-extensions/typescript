import { equal } from "https://deno.land/std@0.207.0/testing/asserts.ts";
import {
    MpSafe,
    SafetyModules
} from "./mod.ts";
const mp_exts: SafetyModules.BaseMpExtension[] = [
    new SafetyModules.MapMpExtension(),
    new SafetyModules.SetMpExtension(),
];
const mp_safe = new MpSafe(mp_exts);
//mp_safe.addExtension(new BigIntMpExtension());

// deno-lint-ignore no-explicit-any
const TestInput: Array<any> = [
    // deno-lint-ignore no-explicit-any
    new Map<string, any>([
        ["test", "data"],
        ["set", new Set([0,1,2,3,4,5])],
        ["array", [0,0,0,0,0]]
    ]),
    //0, BigInt(Number.MAX_SAFE_INTEGER) + 1n,
    //0, BigInt("12345678901234567890"),
    //0, BigInt("10"),
    0, [192,168,0,1],
    0,
    new Set([0,1,2,3,4,5])
];
const encoded = mp_safe.encode(TestInput);
const TestOutput = mp_safe.decode(encoded);
const eq = equal(TestInput, TestOutput)
console.log("TestInput matches TestOutput:", eq);
if(!eq)
    console.log({TestInput, TestOutput})