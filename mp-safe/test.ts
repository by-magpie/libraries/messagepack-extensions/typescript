import { equal } from "https://deno.land/std@0.207.0/testing/asserts.ts";
import {
    MpSafe,
    SafetyModules
} from "./mod.ts";
import { BigIntMpExtension } from "./modules/bigint.ts";
const bie = new BigIntMpExtension();
const mp_exts: SafetyModules.BaseMpExtension[] = [
    bie,
];
const mp_safe = new MpSafe(mp_exts);

const TestInput = BigInt(Number.MAX_SAFE_INTEGER) + 1n;
let passA = false;
{

    const encoded = bie.encode(TestInput);//mp_safe.encode(TestInput);
    if(!encoded)
        throw new Error("Null")
    console.log(encoded)
    const TestOutput = bie.decode(encoded)//mp_safe.decode(encoded);
    const eq = equal(TestInput, TestOutput)
    console.log("TestInput matches TestOutput:", eq);
    if(!eq)
        console.log({TestInput, TestOutput});
    else
        passA = true;
}
console.log("=========b")
if(passA){

    const encoded = mp_safe.encode(TestInput);
    if(!encoded)
        throw new Error("Null")
    console.log(encoded)
    const TestOutput = mp_safe.decode(encoded);
    const eq = equal(TestInput, TestOutput)
    console.log("TestInput matches TestOutput:", eq);
    if(!eq)
        console.log({TestInput, TestOutput})
}