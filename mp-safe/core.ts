import { encode, decode, ExtensionCodec } from "../msgpack_import.ts";
import { BaseMpExtension } from "./modules/base.ts"
export class MpSafe {
    codec: ExtensionCodec;
    constructor(exts: Array<BaseMpExtension>){
        this.codec = new ExtensionCodec();
        this.addExtensions(exts);
    }
    addExtensions(exts: BaseMpExtension[]){
        for(const ext of exts){
            this.addExtension(ext);
        }
    }
    addExtension(ext: BaseMpExtension){
        ext.loadED(
            (d)=>this.encode(d),
            (d)=>this.decode(d)
        )
        ext.install(this.codec);
    }
    encode(data: unknown): Uint8Array {
        return encode(data, { extensionCodec: this.codec });
    }
    decode(data: Uint8Array): unknown {
        return decode(data, { extensionCodec: this.codec });
    }
}